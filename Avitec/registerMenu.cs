using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Avitec.com.lauraestil;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace Avitec
{
    [Activity(Label = "registerMenu")]
    public class registerMenu : Android.App.Activity
    {
        AvitecWS service = new AvitecWS();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            this.SetContentView(Resource.Layout.registerLayout);

            Button buttonRegistra = (Button)FindViewById<Button>(Resource.Id.ButtonGoRegister);

            TextView labelText = (TextView)FindViewById<TextView>(Resource.Id.editTextUserName);
            TextView labelPass1 = (TextView)FindViewById<TextView>(Resource.Id.editTextPass);
            TextView labelMail1 = (TextView)FindViewById<TextView>(Resource.Id.editTextMail1);
            TextView labelPass2 = (TextView)FindViewById<TextView>(Resource.Id.editTextPassConfirm);

            buttonRegistra.Click += delegate
            {            
                string mail = labelMail1.Text;
                string passEncripted = MD5Hash(labelPass1.Text);
                string userName = labelText.Text;

                if (!isValidName(userName))
                {
                    showMessage("El nombre de usuario no debe contener espacios ni car�cteres especiales");
                    labelText.Error = "El nombre de usuario no debe contener espacios ni car�cteres especiales";
                }
                else if (checkIfUserExist(userName))
                {
                    showMessage("El nombre de usuario introducido ya existe.");
                    labelText.Error = "El nombre de usuario introducido ya existe.";
                }
                else if (!matchValues(labelPass1.Text, labelPass2.Text))
                {
                    showMessage("Las contrase�as no coinciden, por favor revisa los valores introducidos");
                    labelPass1.Error = "Las contrase�as no coinciden";
                    labelPass2.Error = "Las contrase�as no coinciden";
                }
                else if (!Regex.Match(labelMail1.Text,
                                        @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
                {
                    showMessage("El correo electr�nico introducido no es v�lido, por favor revisa los valores introducidos");
                    labelMail1.Error = "El correo electr�nico introducido no es v�lido";

                }
                else if (!service.InsertData(userName, passEncripted, mail))
                {
                    showMessage("Lamentablemente algo ha ido mal :( Por favor vuelve a intentarlo m�s tarde");
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Bienvenido/a!");
                    sb.Append("\n\n");
                    sb.Append("Acabas de registrarte en nuestra aplicaci�n, ahora");
                    sb.Append("\n");
                    sb.Append("puedes disfrutar de todo su contenido y de todas nuestras utilidades.");
                    sb.Append("\n");
                    sb.Append("Recueda que tu nombre de usuario es: ");
                    sb.Append(userName);
                    sb.Append("\n");
                    sb.Append("y tu contrase�a es: ");
                    sb.Append(labelPass1.Text);

                    //envia mail a trav�s del servicio web
                    service.SendMail(mail, "Bienvenido/a a Avitec!", sb.ToString());

                    showMessage("Te has registrado correctamente, por favor no olvides revisar tu correo electr�nico");

                    //TODO: resaltar campos invalidos
                    //TODO: pol�tica de t�rminos de uso
                    //TODO: Entrar en la aplicaci�n
                    //TODO: Implementar registro usando las API's de Facebook y Google+ 
                    //TODO: Continuar a partir de aqu�, seguir programando la aplicaci�n


                }

  
            };
        }

        /**
         * Encripta contrase�as
         */ 
        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        /**
         * Muesta mensaje como ventana emergente
         */
        public void showMessage(String name)
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetTitle("Atenci�n");
            alert.SetMessage(name);
            alert.SetPositiveButton("Aceptar", (senderAlert, Args) =>
            {

            });
            //run the alert in UI thread to display in the screen
            RunOnUiThread(() =>
            {
                alert.Show();
            });
        }
        
        /**
         * Comprueba que los valores coinciden
         */
        public bool matchValues(string pass1, string pass2)
        {
            bool resul = false;

            if (pass1.Equals(pass2))
            {
                resul = true;
            }

            return resul;
        }

        /**
         * Comprueba que el nombre no tiene espacios
         * */
        public bool isValidName(string name)
        {
            bool resul = false;

            Regex rgx = new Regex(@"\s");

            if (!rgx.IsMatch(name)) 
            {
                resul = true;
            }

            return resul;
        }

        /**
         * Comprueba que el usuario existe en la BD
         * */
        public bool checkIfUserExist(string userName)
        {
            bool resul = false;

            if (service.userExist(userName))
            {
                resul = true;          
            }

            return resul;
        }
    }
}