﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Avitec.com.lauraestil;
using System.Text.RegularExpressions;
using System.Text;
using System.Security.Cryptography;


namespace Avitec
{
    [Activity(Label = "Avitec", MainLauncher = true, Icon = "@drawable/icon")]

    public class MainActivity : Activity
    {

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
             
            /*Bind the click event to the method that launch the activity
             * it fixes the NullReferenceException between activities controls  
             * */
            this.FindViewById<Button>(Resource.Id.ButtonRegistraNuevo).Click 
                += this.startRegisterActivity;
                 
        }

        public void startRegisterActivity(object sender, EventArgs e)
        {
            this.StartActivity(typeof(Avitec.registerMenu));
        }

        public void showMessage(String name)
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetTitle("Information");
            alert.SetMessage(name);
            alert.SetPositiveButton("Aceptar", (senderAlert, Args) =>
            {

            });
            //run the alert in UI thread to display in the screen
            RunOnUiThread(() =>
            {
                alert.Show();
            });
        }

    }
}

